/* eslint no-console: 0 */
const { ApolloServer } = require('apollo-server');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const configDB = require('./config/database.js');

const typeDefs = require('./app/graphql/schemas');
const resolvers = require('./app/graphql/resolvers');
// const jwt = require('./app/helpers/jwt');

dotenv.config({ path: '.env' });

mongoose
  .connect(
    configDB.url,
    {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    },
  )
  .then(() => console.log('MongoDB connected'))
  .catch((err) => console.log(err));

// async function getUser(req) {
//   const authorization = req.headers.authorization || '';
//   if (authorization === '') return false;
//   if (!authorization.startsWith('Bearer ')) return false;
//   const token = authorization.slice(7, authorization.length);
//   const payload = await jwt.parse(token);
//   if (payload === false) return false;
//   const user = await User.query().findOne({ id: payload.userId });
//   if (!user) return false;
//   return user;
// }

const server = new ApolloServer({
  typeDefs,
  resolvers,
  // context: async ({ req }) => {
  //   const user = await getUser(req);
  //   return { user };
  // },
});

server.listen().then(({ url }) => {
  console.log(`GraphQL server ready at ${url}`);
});
