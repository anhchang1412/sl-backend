const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  commentId: 'string',
  postId: 'string',
  content: 'string',
  label: 'string',
  createAt: 'string',
  userName: 'string',
  userAvatar: 'string',
});
const Comment = mongoose.model('Tank', schema);

module.exports = Comment;
