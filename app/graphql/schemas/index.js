const { gql } = require('apollo-server');

const typeDefs = gql`
  type Comment {
    commentId: String,
    postId: String,
    content: String,
    label: String,
    createAt: String,
    userName: String,
    userAvatar: String,
  }

  type Query {
    hi: String
    getComment: [Comment]
  }

  input AddComment {
    commentId: String,
    postId: String,
    content: String,
    label: String,
    createAt: String,
    userName: String,
    userAvatar: String,
  }

  type Mutation {
    addComment(AddComment: AddComment): Comment
  }
`;

module.exports = typeDefs;
