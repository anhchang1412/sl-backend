/* eslint global-require:0, import/no-dynamic-require: 0 */
const fs = require('fs');
const path = require('path');

const resolvers = {};

fs.readdirSync(__dirname)
  .filter((file) => (file.indexOf('.') !== 0) && (file !== 'index.js'))
  .forEach((dir) => {
    resolvers[dir] = {};
    fs.readdirSync(path.join(__dirname, dir))
      .forEach((file) => {
        const resolver = require(path.join(__dirname, dir, file));
        const name = path.basename(file, '.js');
        resolvers[dir][name] = resolver;
      });
  });

module.exports = {
  Query: {
    hi: resolvers.auth.hi,
    getComment: resolvers.comment.getComment,
  },

  Mutation: {
    addComment: resolvers.comment.addComment,
  },
};
