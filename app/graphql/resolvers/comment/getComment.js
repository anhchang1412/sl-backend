const { Comment } = require('../../../database/model');

const getComment = async () => {
  const comments = await Comment.find({});
  return comments;
};

module.exports = getComment;
