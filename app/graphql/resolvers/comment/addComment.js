const { Comment } = require('../../../database/model');

const addComment = async (parent, args) => {
  const comments = await Comment.create(args.AddComment);
  return comments;
};

module.exports = addComment;
