const Enum = require('./Enum.js');

module.exports = new Enum({
  INACTIVE: 0,
  MEMBER: 3,
  ADMIN: 7,
});
